# dalsa_gige_driver

[![Build Status](https://gitlab.drone.camhd.science/api/badges/apl-ocean-engineering/dalsa_gige_driver/status.svg)](https://gitlab.drone.camhd.science/apl-ocean-engineering/dalsa_gige_driver)

ROS Driver for Dalsa Teledyne Nano GigE cameras, based on their [GigE-V Framework for Linux](https://www.teledynedalsa.com/en/support/downloads-center/software-development-kits/132/) (download requires registration).

Once installed, the GigEv framework will install a set up ENV variables: `GIGEV_DIR`, `GENICAM_ROOT_V3_0` etc.  If there are build issues, check that those env variables are present, potentially re-logging in.

This package required `libcap-dev`.

Based / inspired heavily by the ROS [prosilica_driver](https://github.com/ros-drivers/prosilica_driver).

This driver requires privilges `cap_net_raw` and `cap_sys_nice`. Rather than running as root, we typically use [dalsa_gige_grant](https://gitlab.com/apl-ocean-engineering/dalsa_gige_grant). This is not a ROS package, and must be installed separately.

# Status

Tested and working on our cameras (Genie Nano C2420), so probably not feature complete. Bug reports and patches are welcome!

# License

This software is released under a [BSD license](LICENSE).

<!--
// To build and test with gdb
// catkin build --cmake-args -DCMAKE_BUILD_TYPE=Debug && roslaunch dalsa_gige_driver left_gdb.launch
-->
