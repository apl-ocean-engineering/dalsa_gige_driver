This repo builds two Docker images:

* [`registry.gitlab.com/apl-ocean-engineering/dalsa_gige_ros_docker:build-latest`](registry.gitlab.com/apl-ocean-engineering/dalsa_gige_ros_docker:build-latest) adds the Dalsa GigE SDK to the [ROS "noetic-perception" image](https://hub.docker.com/_/ros/).  The entrypoint is set to the [`ros_entrypoint.sh`](ros_entrypoint.sh) which automatically sources the ROS environment.

* [`registry.gitlab.com/apl-ocean-engineering/dalsa_gige_ros_docker:latest`](registry.gitlab.com/apl-ocean-engineering/dalsa_gige_ros_docker:latest) installs the [`dalsa_gige_driver` ROS1 node](https://gitlab.com/apl-ocean-engineering/dalsa_gige_driver) and its dependencies in a catkin workspace at `/root/arena_ws`.   The workspace is built and sources by `ros_entrypoint.sh`.   The default command in the images is
`roslaunch dalsa_gige dalsa_gige_nodelet.launch
` which is a minimal example for connecting to a single Lucid Vision camera on the network (see "Running").


## **Dockerfile**

The Docker images are built automatically by Gitlab CI and can be retrieved as:

* [`registry.gitlab.com/apl-ocean-engineering/dalsa_gige_ros_docker:latest`](registry.gitlab.com/apl-ocean-engineering/dalsa_gige_ros_docker:latest)
* [`registry.gitlab.com/apl-ocean-engineering/dalsa_gige_ros_docker:build-latest`](registry.gitlab.com/apl-ocean-engineering/dalsa_gige_ros_docker:build-latest)


The Docker images can be built manually with the `./build.sh` script, which will use the latest version of [`dalsa_gige_ros`](https://github.com/lucidvisionlabs/dalsa_gige_ros) from Gitlab.  The following build args can be used to set the Git repo:

* `dalsa_gige_ROS_REPO` : URL for git repo for `dalsa_gige_ros`.  Defaults to the [official Lucid Vision repo on Github](https://github.com/lucidvisionlabs/dalsa_gige_ros)
* `dalsa_gige_ROS_BRANCH` : Branch in repo to checkout



`./build.sh local` will copy the contents of the `local_src` directory into the image before building, allowing development code to be built in the image.


# Running

The default command will run [dalsa_gige_nodelet.launch](dalsa_gige_ros/dalsa_gige/launch/dalsa_gige_nodelet.launch).

```
docker run --net host --rm -it registry.gitlab.com/apl-ocean-engineering/dalsa_gige_ros_docker:latest
```

For more sophisticated launch behavior, other launch or configuration files can be mounted in the image.
