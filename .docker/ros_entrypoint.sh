#!/bin/bash
#set -e

# Load the GigE-V environment variables
source /etc/profile.d/td_gigevapi.sh
source /etc/profile.d/td_genapi_v3_0.sh

ROS_VERSION=${ROS_VERSION:-noetic}

# setup ros environment
if [ "${ROS_WS}" == "" ]; then
    echo "Loading default environment: /opt/ros/$ROS_VERSION/setup.bash"
    source /opt/ros/$ROS_VERSION/setup.bash
else
    echo "Loading environment from workspace: $ROS_WS"
    source $ROS_WS/devel/setup.bash
fi

exec "$@"
