/*********************************************************************
 *
 * Driver for Dalsa Gig-E cameras using GigE-V API.
 *
 * Based on prosilica driver which is licensed as follows:
 *
 * Software License Agreement (BSD License)
 *
 *  Dalsa_GigE Copyright (c) 2020, University of Washington;
 *  based on
 *  prosilica_driver Copyright (c) 2008, Willow Garage, Inc.
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * * Neither the name of the Willow Garage nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
#include "dalsa_gige_driver/dalsa_camera_nodelet.h"

#include <imaging_msgs/ImagingMetadata.h>
#include <pluginlib/class_list_macros.h>

#include "dalsa_gige_driver/dalsa_camera_factory.h"

namespace dalsa_gige_driver {

int DalsaCameraNodelet::_numCameras = 0;

DalsaCameraNodelet::DalsaCameraNodelet(const std::string &name)
    : Nodelet(),
      name_(name),
      camera_(nullptr),
      imaging_metadata_pub_(),
      ros_pix_encoding_("auto") {
  if (_numCameras == 0) {
    GevApiInitialize();
  }
  ++_numCameras;
}

DalsaCameraNodelet::~DalsaCameraNodelet() {
  if (camera_) {
    camera_->stop();
    camera_.reset();
  }

  trigger_sub_.shutdown();
  // poll_srv_.shutdown();
  image_publisher_.shutdown();

  --_numCameras;
  if (_numCameras <= 0) {
    GevApiUninitialize();
    _numCameras = 0;
  }
}

void DalsaCameraNodelet::onInit() {
  auto nh = getMTNodeHandle();
  auto pnh = getMTPrivateNodeHandle();

  //~~ Retrieve parameters related to connecting to the camera
  //   and setting up the API during conenction
  //   (not camera params ... those are primarily set through dyncfg)

  pnh.param<std::string>("frame_id", frame_id_, "/camera_optical_frame");
  ROS_INFO("Loaded param frame_id: %s", frame_id_.c_str());

  pnh.param<std::string>("ip_address", ip_address_, "");
  ROS_INFO("Loaded ip address: %s", ip_address_.c_str());

  pnh.param<std::string>("serial_number", serial_number_, "");
  ROS_INFO("Loaded serial number: %s", serial_number_.c_str());

  pnh.param<int>("packet_size", camera_configuration_.packet_size, -1);
  if (camera_configuration_.packet_size > 0) {
    ROS_INFO("Packet size set to: %d", camera_configuration_.packet_size);
  }

  pnh.param<int>("cpu_affinity", camera_configuration_.cpu_affinity, -1);
  if (camera_configuration_.cpu_affinity > 0) {
    ROS_INFO("CPU affinity set to: %d", camera_configuration_.cpu_affinity);
  }

  if (ip_address_.empty() && serial_number_.empty()) {
    ROS_FATAL("Dalsa: Must specify either ip_address or serial_number");
  }
  // NOTE(lindzey): I thought about modifying openCamera() to try SN if IP
  //    fails, but decided that I didn't like having redundant
  //    specifications.
  if (!ip_address_.empty() && !serial_number_.empty()) {
    ROS_WARN(
        "Dalsa: Specified both ip_address and serial_number; will only use IP");
  }

  pnh.param<std::string>("camera_info_url", calibration_url_, "");
  ROS_INFO("Using calibration URL: %s", calibration_url_.c_str());

  updater_.add(name_.c_str(), this, &DalsaCameraNodelet::getCurrentState);

  // Advertise topics
  ros::NodeHandle imagenh(nh);
  image_transport::ImageTransport image_it(imagenh);
  image_publisher_ = image_it.advertiseCamera("image_raw", 1);

  dalsa_imaging_metadata_pub_ =
      nh.advertise<dalsa_gige_msgs::DalsaImagingMetadata>(
          "dalsa_imaging_metadata", 10);

  imaging_metadata_pub_ =
      nh.advertise<imaging_msgs::ImagingMetadata>("imaging_metadata", 10);

  // Set camera info manager
  if (calibration_url_.size() > 0) {
    ROS_INFO_STREAM("Loading calibration " << calibration_url_);
    info_manager_ = boost::shared_ptr<camera_info_manager::CameraInfoManager>(
        new camera_info_manager::CameraInfoManager(pnh, frame_id_));

    if (!info_manager_->loadCameraInfo(calibration_url_)) {
      ROS_WARN_STREAM("Unable to load info from " << calibration_url_);
    }
  }

  // Open camera -- this function will block until a camera is found!
  if (!openCamera()) {
    pnh.param<int>("opencamera__retry_period", opencamera__retry_period_, 3.);
    ROS_INFO("Continuing to try to connect with retry period of %d sec",
             opencamera__retry_period_);
    connect_retry_timer_ =
        nh.createTimer(ros::Duration(opencamera__retry_period_),
                       [&](const ros::TimerEvent &event) {
                         if (openCamera()) {
                           connect_retry_timer_.stop();
                         }
                       });
  }
}

// Attempt to connect to the camera. If an IP is provided this ignores the SN.
bool DalsaCameraNodelet::openCamera() {
  boost::lock_guard<boost::recursive_mutex> scoped_lock(config_mutex_);
  camera_state_ = OPENING;

  if (!ip_address_.empty()) {
    ROS_INFO_STREAM("Trying to connect to camera with ipaddress "
                    << ip_address_);

    auto cam = DalsaCameraFactory::OpenByAddr(ip_address_.c_str(),
                                              camera_configuration_);

    if (cam) {
      camera_ = cam.value();
      updater_.update();
    } else {
      auto status = cam.error();
      ROS_INFO_STREAM("Unable to connect to camera, got status "
                      << status << ": " << stringFromDalsaStatus(status));
      return false;
    }
  } else if (!serial_number_.empty()) {
    ROS_INFO_STREAM("Trying to connect to camera with serial number "
                    << serial_number_);

    auto cam = DalsaCameraFactory::OpenBySN(serial_number_.c_str());

    if (cam) {
      camera_ = cam.value();
      updater_.update();
    } else {
      auto status = cam.error();
      ROS_INFO_STREAM("Unable to connect to camera, got status "
                      << status << ": " << stringFromDalsaStatus(status));
      return false;
    }
  }

  // ~~~ Do a bunch of stuff once a camera is opened ~~~

  auto nh = getNodeHandle();
  auto pnh = getPrivateNodeHandle();

  // Disable throughput limit
  camera_->setThroughputLimit(0);

  // Setup periodic callback to get new status data from the camera
  float update_rate = 1.0;
  update_timer_ =
      nh.createTimer(ros::Rate(update_rate).expectedCycleTime(),
                     &DalsaCameraNodelet::updateCallback, this, false, false);

  // Setup dynamic reconfigure server
  configure_server_.reset(new ReconfigureServer(config_mutex_, pnh));
  configure_server_->setCallback(
      boost::bind(&DalsaCameraNodelet::reconfigureCallback, this, _1, _2));

  return true;
}

void DalsaCameraNodelet::start() {
  if (!camera_) return;

  camera_->setFrameCallback(
      boost::bind(&DalsaCameraNodelet::publishImage, this, _1));

  GEV_STATUS status = camera_->start();
  if (status != GEVLIB_OK) {
    ROS_ERROR_STREAM("Error when starting camera "
                     << status << ": " << stringFromDalsaStatus(status));
    return;
  }
  update_timer_.start();

  ROS_INFO("Done starting camera");
}

void DalsaCameraNodelet::stop() {
  update_timer_.stop();

  if (!camera_) return;

  camera_->stop();
}

/// \brief Callback from DalsaGigE::Camera whenever a frame is Received
///
///  Remember this runs in the **Camera acquisition thread**
///
void DalsaCameraNodelet::publishImage(GEV_BUFFER_OBJECT *buffer) {
  publishImage(buffer, ros::Time::now());
}

void DalsaCameraNodelet::publishImage(GEV_BUFFER_OBJECT *buffer,
                                      ros::Time time) {
  camera_state_ = OK;
  state_info_ = "Camera operating normally";
  if (image_publisher_.getNumSubscribers() > 0) {
    if (buffer->status != 0) {
      ROS_ERROR_STREAM("Got non-zero status in GEV buffer "
                       << buffer->status << ": "
                       << stringFromDalsaStatus(buffer->status));
      return;
    }

    // For now, re-publish raw image _as is_ with appropriate image
    // encoding tag.

    sensor_msgs::Image::Ptr img(new sensor_msgs::Image());

    img->header.stamp = time;
    img->header.frame_id = frame_id_;

    // nicked this from cv_bridge
    img->is_bigendian =
        (boost::endian::order::native == boost::endian::order::big);

    // GigE-V will pre-process some data types during acquisition
    // (e.g. unpacking packed data), so the _actual_ data format in the
    // buffer will sometimes not be the same as the acquired data.
    const uint32_t unpackedFmt = GevGetConvertedPixelType(0, buffer->format);

    ROS_DEBUG_THROTTLE(1, "Dalsa buffer format: %08x;  after unpacking: %08x",
                       buffer->format, unpackedFmt);

    if (ros_pix_encoding_ == "auto") {
      img->encoding = DalsaFormatToRosFormat(unpackedFmt, camera_->getXFlip(),
                                             camera_->getYFlip());
      ROS_DEBUG_STREAM_THROTTLE(
          1, "Automatically converted format "
                 << unpackedFmt << " to ROS pixel format: " << img->encoding);
    } else {
      img->encoding = ros_pix_encoding_;
      ROS_DEBUG_STREAM_THROTTLE(
          1, "Using user-specified pixel format: " << img->encoding);
    }

    if (img->encoding.empty()) {
      ROS_ERROR("Couldn't figure ROS encoding for Dalsa format 0x%08x",
                unpackedFmt);
      return;
    }

    img->height = buffer->h;
    img->width = buffer->w;

    img->step = img->width * GevGetPixelSizeInBytes(unpackedFmt);

    const size_t size = img->step * img->height;
    img->data.resize(size);

    // !! Yes!  I am doing an image copy!
    memcpy(reinterpret_cast<char *>(&img->data[0]), buffer->address, size);

    sensor_msgs::CameraInfo::Ptr ci;

    if (info_manager_) {
      ci.reset(new sensor_msgs::CameraInfo(info_manager_->getCameraInfo()));

      // \todo{amarburg} Validate image size against size in camerainfo
      ROS_DEBUG_STREAM_COND(
          (ci->height != img->height) || (ci->width != img->width),
          "Size of camera info doesn't agree with data from camera");
    } else {
      // Path
      ci.reset(new sensor_msgs::CameraInfo());

      // Create a minimal camerainfo
      ci->height = img->height;
      ci->width = img->width;
    }

    if (ci) {
      ci->header.stamp = img->header.stamp;
      ci->header.frame_id = img->header.frame_id;
      image_publisher_.publish(img, ci);
    }
  }
}

void DalsaCameraNodelet::updateCallback(const ros::TimerEvent &event) {
  if (!camera_) return;

  // Download the most recent data from the device
  const float exposure = camera_->getExposure();
  const float analogGain = camera_->getAnalogGain();
  const float digitalGain = camera_->getDigitalGain();

  ROS_DEBUG("Exposure = %f us, analog gain = %f, digital gain = %.1f", exposure,
            analogGain, digitalGain);

  const auto now = ros::Time::now();

  {
    dalsa_gige_msgs::DalsaImagingMetadata msg;
    msg.header.stamp = now;
    msg.header.frame_id = frame_id_;
    msg.exposure_us = exposure;
    msg.auto_gain = camera_->getAutoGain();
    msg.auto_exposure = camera_->getAutoExposure();
    msg.digital_gain = digitalGain;
    msg.sensor_gain = analogGain;
    msg.serial_number = serial_number_;

    msg.red_gain = camera_->getRedGain();
    msg.blue_gain = camera_->getBlueGain();
    msg.green_gain = camera_->getGreenGain();

    msg.gamma = camera_->getGamma();

    dalsa_imaging_metadata_pub_.publish(msg);
  }

  {
    imaging_msgs::ImagingMetadata msg;
    msg.header.stamp = now;
    msg.header.frame_id = frame_id_;
    msg.exposure_us = exposure;
    msg.gain = analogGain;

    imaging_metadata_pub_.publish(msg);
  }

  camera_state_ = OK;
  state_info_ = "Camera operating normally";
}

void DalsaCameraNodelet::reconfigureCallback(
    dalsa_gige::DalsaCameraConfig &config, uint32_t level) {
  ROS_INFO("Processing reconfigure request");

  const auto stop_level =
      (uint32_t)dynamic_reconfigure::SensorLevels::RECONFIGURE_STOP;
  if (camera_->isRunning() && (level >= stop_level)) {
    ROS_INFO("Stopping sensor for reconfigure");
    stop();
  }

  TriggerConfiguration triggerConfig;

  if ((config.trigger_mode != last_config_.trigger_mode) ||
      (config.frame_rate != last_config_.frame_rate)) {
    if (config.trigger_mode == "streaming") {
      triggerConfig.mode = TriggerConfiguration::TriggerMode::Streaming;
      triggerConfig.frameRate = 0;
    } else if (config.trigger_mode == "fixedrate") {
      triggerConfig.mode = TriggerConfiguration::TriggerMode::FixedRate;
      triggerConfig.frameRate = config.frame_rate;
    } else if (config.trigger_mode == "software") {
      ROS_ERROR("'software' trigger mode isn't supported");
    } else if (config.trigger_mode == "inputline1") {
      triggerConfig.mode = TriggerConfiguration::TriggerMode::InputLine1;
      triggerConfig.frameRate = 0;
    } else if (config.trigger_mode == "inputline2") {
      triggerConfig.mode = TriggerConfiguration::TriggerMode::InputLine2;
      triggerConfig.frameRate = 0;
    } else {
      ROS_ERROR("Invalid trigger mode '%s' in reconfigure request",
                config.trigger_mode.c_str());
    }

    ROS_INFO_STREAM("Setting trigger mode " << config.trigger_mode);
    GEVLIB_STATUS status = camera_->setTrigger(triggerConfig);

    if (status != GEVLIB_OK) {
      ROS_ERROR_STREAM("Error setting trigger: "
                       << status << ": " << stringFromDalsaStatus(status));
    }

    // Check framerate after checking -- only valid if camera is freerunning
    if ((triggerConfig.mode == TriggerConfiguration::TriggerMode::Streaming) ||
        (triggerConfig.mode == TriggerConfiguration::TriggerMode::FixedRate)) {
      float curr_frame_rate = camera_->getFrameRate();
      if (curr_frame_rate > 0) {
        ROS_INFO_STREAM("Actual camera frame rate: " << curr_frame_rate);
      } else if (curr_frame_rate == -1) {
        ROS_INFO("Camera set to maximum frame rate");
      } else {
        ROS_WARN_STREAM("Error retrieving frame rate (" << curr_frame_rate
                                                        << ")");
      }
    }
  }

  if (config.output1_mode != last_config_.output1_mode) {
    ROS_INFO_STREAM("Setting output 1 to " << config.output1_mode);
    reconfigureOutput(OutputConfiguration::OutputLine::Output1,
                      config.output1_mode);
  }

  if (config.output2_mode != last_config_.output2_mode) {
    ROS_INFO_STREAM("Setting output 2 to " << config.output2_mode);
    reconfigureOutput(OutputConfiguration::OutputLine::Output2,
                      config.output2_mode);
  }

  if (config.turbo_drive != last_config_.turbo_drive) {
    ROS_INFO("Set turbo drive to %s", (config.turbo_drive ? "True" : "False"));
    camera_->setTurboDrive(config.turbo_drive);
  }

  if ((config.auto_exposure != last_config_.auto_exposure) ||
      (config.auto_analog_gain != last_config_.auto_analog_gain) ||
      (config.analog_gain != last_config_.analog_gain) ||
      (config.exposure_ms != last_config_.exposure_ms)) {
    ROS_INFO("Setting auto exposure %s; auto gain %s",
             config.auto_exposure ? "ON" : "OFF",
             config.auto_analog_gain ? "ON" : "OFF");

    {
      GEVLIB_STATUS status =
          camera_->setExposure(config.auto_exposure, config.exposure_ms);
      if (status != GEVLIB_OK) {
        ROS_WARN_STREAM("Error setting exposure: "
                        << status << ": " << stringFromDalsaStatus(status));
      }
    }

    {
      GEVLIB_STATUS status =
          camera_->setAnalogGain(config.auto_analog_gain, config.analog_gain);
      if (status != GEVLIB_OK) {
        ROS_WARN_STREAM("Error setting auto gain limits: "
                        << status << ": " << stringFromDalsaStatus(status));
      }
    }
  }

  if (config.digital_gain != last_config_.digital_gain) {
    ROS_INFO_STREAM("Setting digital gain to " << config.digital_gain);
    GEV_STATUS status = camera_->setDigitalGain(config.digital_gain);
    if (status != GEVLIB_OK) {
      ROS_ERROR_STREAM("Unable to set digital gain "
                       << status << ": " << stringFromDalsaStatus(status));
    }
  }

  if (config.red_gain != last_config_.red_gain) {
    ROS_INFO_STREAM("Setting red gain to " << config.red_gain);
    GEV_STATUS status = camera_->setRedGain(config.red_gain);
    if (status != GEVLIB_OK) {
      ROS_ERROR_STREAM("Unable to set red gain "
                       << status << ": " << stringFromDalsaStatus(status));
    }
  }

  if (config.blue_gain != last_config_.blue_gain) {
    ROS_INFO_STREAM("Setting blue gain to " << config.blue_gain);
    GEV_STATUS status = camera_->setBlueGain(config.blue_gain);
    if (status != GEVLIB_OK) {
      ROS_ERROR_STREAM("Unable to set blue gain "
                       << status << ": " << stringFromDalsaStatus(status));
    }
  }

  if (config.gamma != last_config_.gamma) {
    ROS_INFO_STREAM("Setting gamma correction to " << config.gamma);
    GEV_STATUS status = camera_->setGamma(config.gamma);
    if (status != GEVLIB_OK) {
      ROS_ERROR_STREAM("Unable to set gamma " << status << ": "
                                              << stringFromDalsaStatus(status));
    }
  }

  if ((config.flip_x != last_config_.flip_x) ||
      (config.flip_y != last_config_.flip_y)) {
    ROS_INFO_STREAM("Setting image flipping to X: "
                    << (config.flip_x ? "TRUE" : "FALSE")
                    << ", Y: " << (config.flip_y ? "TRUE" : "FALSE"));
    GEV_STATUS status = camera_->setFlip(config.flip_x, config.flip_y);
    if (status != GEVLIB_OK) {
      ROS_ERROR_STREAM("Unable to set flip " << status << ": "
                                             << stringFromDalsaStatus(status));
    }
  }

  ros_pix_encoding_ = config.image_encoding;

  if (!camera_->isRunning() && (level >= stop_level)) {
    ROS_INFO("Starting sensor after configuration");
    start();
  }

  last_config_ = config;
}

void DalsaCameraNodelet::reconfigureOutput(OutputConfiguration::OutputLine line,
                                           const std::string &mode) {
  OutputConfiguration output;
  output.line = line;

  if (mode == "none") {
    output.source = OutputConfiguration::OutputMode::None;
  } else if (mode == "pulseonstartofframe") {
    output.source = OutputConfiguration::OutputMode::PulseOnStartofFrame;
  } else if (mode == "pulseonstartofacquisition") {
    output.source = OutputConfiguration::OutputMode::PulseOnStartofAcquisition;
  }

  GEV_STATUS status = camera_->setOutput(output);

  if (status != GEVLIB_OK) {
    ROS_ERROR_STREAM("Error setting output " << status << ": "
                                             << stringFromDalsaStatus(status));
  }
}

//==== DiagnosticStatus callbacks ====

void DalsaCameraNodelet::getCurrentState(
    diagnostic_updater::DiagnosticStatusWrapper &stat) {
  // stat.add("Serial", guid_);

  stat.add("Info", state_info_);
  stat.add("Intrinsics", intrinsics_);

  if (camera_) {
    stat.add("Total timeouts", camera_->stats.numTimeouts);
    stat.add("Total frames", camera_->stats.numFrames);
  }

  switch (camera_state_) {
    case OPENING:
      stat.summary(diagnostic_msgs::DiagnosticStatus::WARN, "Opening camera");
      break;
    case OK:
      stat.summary(diagnostic_msgs::DiagnosticStatus::OK,
                   "Camera operating normally");
      break;
    case CAMERA_NOT_FOUND:
      stat.summaryf(diagnostic_msgs::DiagnosticStatus::ERROR,
                    "Can not find camera");
      break;
    case FORMAT_ERROR:
      stat.summary(diagnostic_msgs::DiagnosticStatus::ERROR,
                   "Problem retrieving frame");
      break;
    case ERROR:
      stat.summary(diagnostic_msgs::DiagnosticStatus::ERROR,
                   "Camera has encountered an error");
      break;
    default:
      break;
  }
}

// Return the name of the reported error.
// This function was largely auto-generated using a python script and
// the #define block of gevapi.h
std::string DalsaCameraNodelet::stringFromDalsaStatus(GEV_STATUS status) {
  if (status == GEVLIB_OK) {
    return "OK";
  }
  // Standard API errors
  else if (status == GEVLIB_ERROR_GENERIC) {
    // Generic Error. A catch-all for unexpected behaviour.
    return "GEVLIB_ERROR_GENERIC";
  } else if (status == GEVLIB_ERROR_NULL_PTR) {
    // NULL pointer passed to function or the result of a cast operation
    return "GEVLIB_ERROR_NULL_PTR";
  } else if (status == GEVLIB_ERROR_ARG_INVALID) {
    // Passed argument to a function is not valid
    return "GEVLIB_ERROR_ARG_INVALID";
  } else if (status == GEVLIB_ERROR_INVALID_HANDLE) {
    // Invalid Handle
    return "GEVLIB_ERROR_INVALID_HANDLE";
  } else if (status == GEVLIB_ERROR_NOT_SUPPORTED) {
    // This version of hardware/fpga does not support this feature
    return "GEVLIB_ERROR_NOT_SUPPORTED";
  } else if (status == GEVLIB_ERROR_TIME_OUT) {
    // Timed out waiting for a resource
    return "GEVLIB_ERROR_TIME_OUT";
  } else if (status == GEVLIB_ERROR_NOT_IMPLEMENTED) {
    // Function / feature is not implemented.
    return "GEVLIB_ERROR_NOT_IMPLEMENTED";
  } else if (status == GEVLIB_ERROR_NO_CAMERA) {
    // The action can't be execute because the camera is not connected.
    return "GEVLIB_ERROR_NO_CAMERA";
  } else if (status == GEVLIB_ERROR_INVALID_PIXEL_FORMAT) {
    // Pixel Format is invalid (not supported or not recognized)
    return "GEVLIB_ERROR_INVALID_PIXEL_FORMAT";
  } else if (status == GEVLIB_ERROR_PARAMETER_INVALID) {
    // Passed Parameter (could be inside a data structure) is invalid/out of
    // range.
    return "GEVLIB_ERROR_PARAMETER_INVALID";
  } else if (status == GEVLIB_ERROR_SOFTWARE) {
    // software error, unexpected result
    return "GEVLIB_ERROR_SOFTWARE";
  } else if (status == GEVLIB_ERROR_API_NOT_INITIALIZED) {
    // API has not been initialized
    return "GEVLIB_ERROR_API_NOT_INITIALIZED";
  } else if (status == GEVLIB_ERROR_DEVICE_NOT_FOUND) {
    // Device/camera specified was not found.
    return "GEVLIB_ERROR_DEVICE_NOT_FOUND";
  } else if (status == GEVLIB_ERROR_ACCESS_DENIED) {
    // API will not access the device/camera/feature in the specified manner.
    return "GEVLIB_ERROR_ACCESS_DENIED";
  } else if (status == GEVLIB_ERROR_NOT_AVAILABLE) {
    // Feature / function is not available for access (but is implemented).
    return "GEVLIB_ERROR_NOT_AVAILABLE";
  } else if (status == GEVLIB_ERROR_NO_SPACE) {
    // The data being written to a feature is too large for the feature to
    // store.
    return "GEVLIB_ERROR_NO_SPACE";
  } else if (status == GEVLIB_ERROR_XFER_NOT_INITIALIZED) {
    // Payload transfer is not initialized but is required to be for this
    // function.
    return "GEVLIB_ERROR_XFER_NOT_INITIALIZED";
  } else if (status == GEVLIB_ERROR_XFER_ACTIVE) {
    // Payload transfer is active but is required to be inactive for this
    // function.
    return "GEVLIB_ERROR_XFER_ACTIVE";
  } else if (status == GEVLIB_ERROR_XFER_NOT_ACTIVE) {
    // Payload transfer is not active but is required to be active for this
    // function.
    return "GEVLIB_ERROR_XFER_NOT_ACTIVE";
  }
  // Resource errors
  else if (status == GEVLIB_ERROR_SYSTEM_RESOURCE) {
    // Error creating a system resource
    return "GEVLIB_ERROR_SYSTEM_RESOURCE";
  } else if (status == GEVLIB_ERROR_INSUFFICIENT_MEMORY) {
    // error allocating memory
    return "GEVLIB_ERROR_INSUFFICIENT_MEMORY";
  } else if (status == GEVLIB_ERROR_INSUFFICIENT_BANDWIDTH) {
    // Not enough bandwidth to perform operation and/or acquisition
    return "GEVLIB_ERROR_INSUFFICIENT_BANDWIDTH";
  } else if (status == GEVLIB_ERROR_RESOURCE_NOT_ALLOCATED) {
    // Resource is not currently allocated
    return "GEVLIB_ERROR_RESOURCE_NOT_ALLOCATED";
  } else if (status == GEVLIB_ERROR_RESOURCE_IN_USE) {
    // resource is currently being used.
    return "GEVLIB_ERROR_RESOURCE_IN_USE";
  } else if (status == GEVLIB_ERROR_RESOURCE_NOT_ENABLED) {
    // The resource(feature) is not enabled
    return "GEVLIB_ERROR_RESOURCE_NOT_ENABLED";
  } else if (status == GEVLIB_ERROR_RESOURCE_NOT_INITIALIZED) {
    // Resource has not been intialized.
    return "GEVLIB_ERROR_RESOURCE_NOT_INITIALIZED";
  } else if (status == GEVLIB_ERROR_RESOURCE_CORRUPTED) {
    // Resource has been corrupted
    return "GEVLIB_ERROR_RESOURCE_CORRUPTED";
  } else if (status == GEVLIB_ERROR_RESOURCE_MISSING) {
    // A resource (ie.DLL) needed could not located
    return "GEVLIB_ERROR_RESOURCE_MISSING";
  } else if (status == GEVLIB_ERROR_RESOURCE_LACK) {
    // Lack of resource to perform a request.
    return "GEVLIB_ERROR_RESOURCE_LACK";
  } else if (status == GEVLIB_ERROR_RESOURCE_ACCESS) {
    // Unable to correctly access the resource.
    return "GEVLIB_ERROR_RESOURCE_ACCESS";
  } else if (status == GEVLIB_ERROR_RESOURCE_INVALID) {
    // A specified resource does not exist.
    return "GEVLIB_ERROR_RESOURCE_INVALID";
  } else if (status == GEVLIB_ERROR_RESOURCE_LOCK) {
    // resource is currently lock
    return "GEVLIB_ERROR_RESOURCE_LOCK";
  } else if (status == GEVLIB_ERROR_INSUFFICIENT_PRIVILEGE) {
    // Need administrator privilege.
    return "GEVLIB_ERROR_INSUFFICIENT_PRIVILEGE";
  } else if (status == GEVLIB_ERROR_RESOURCE_WRITE_PROTECTED) {
    // No data can be written to the resource
    return "GEVLIB_ERROR_RESOURCE_WRITE_PROTECTED";
  } else if (status == GEVLIB_ERROR_RESOURCE_INCOHERENCY) {
    // The required resources are not valid together
    return "GEVLIB_ERROR_RESOURCE_INCOHERENCY";
  }
  // Data errors
  else if (status == GEVLIB_ERROR_DATA_NO_MESSAGES) {
    // no more messages (in fifo, queue, input stream etc)
    return "GEVLIB_ERROR_DATA_NO_MESSAGES";
  } else if (status == GEVLIB_ERROR_DATA_OVERFLOW) {
    // data could not be added to fifo, queue, stream etc.
    return "GEVLIB_ERROR_DATA_OVERFLOW";
  } else if (status == GEVLIB_ERROR_DATA_CHECKSUM) {
    // checksum validation fail
    return "GEVLIB_ERROR_DATA_CHECKSUM";
  } else if (status == GEVLIB_ERROR_DATA_NOT_AVAILABLE) {
    // data requested isn't available yet
    return "GEVLIB_ERROR_DATA_NOT_AVAILABLE";
  } else if (status == GEVLIB_ERROR_DATA_OVERRUN) {
    // data requested has been overrun by newer data
    return "GEVLIB_ERROR_DATA_OVERRUN";
  } else if (status == GEVLIB_ERROR_DATA_XFER_ABORT) {
    // transfer of requested data did not finish
    return "GEVLIB_ERROR_DATA_XFER_ABORT";
  } else if (status == GEVLIB_ERROR_DATA_INVALID_HEADER) {
    // data header is invalid.
    return "GEVLIB_ERROR_DATA_INVALID_HEADER";
  } else if (status == GEVLIB_ERROR_DATA_ALIGNMENT) {
    // data is not correctly align.
    return "GEVLIB_ERROR_DATA_ALIGNMENT";
  }
  // Ethernet errors
  else if (status == GEVLIB_ERROR_CONNECTION_DROPPED) {
    return "GEVLIB_ERROR_CONNECTION_DROPPED";
  } else if (status == GEVLIB_ERROR_ANSWER_TIMEOUT) {
    return "GEVLIB_ERROR_ANSWER_TIMEOUT";
  } else if (status == GEVLIB_ERROR_SOCKET_INVALID) {
    return "GEVLIB_ERROR_SOCKET_INVALID";
  } else if (status == GEVLIB_ERROR_PORT_NOT_AVAILABLE) {
    return "GEVLIB_ERROR_PORT_NOT_AVAILABLE";
  } else if (status == GEVLIB_ERROR_INVALID_IP) {
    return "GEVLIB_ERROR_INVALID_IP";
  } else if (status == GEVLIB_ERROR_INVALID_CAMERA_OPERATION) {
    return "GEVLIB_ERROR_INVALID_CAMERA_OPERATION";
  } else if (status == GEVLIB_ERROR_INVALID_PACKET) {
    return "GEVLIB_ERROR_INVALID_PACKET";
  } else if (status == GEVLIB_ERROR_INVALID_CONNECTION_ATTEMPT) {
    return "GEVLIB_ERROR_INVALID_CONNECTION_ATTEMPT";
  } else if (status == GEVLIB_ERROR_PROTOCOL) {
    return "GEVLIB_ERROR_PROTOCOL";
  } else if (status == GEVLIB_ERROR_WINDOWS_SOCKET_INIT) {
    return "GEVLIB_ERROR_WINDOWS_SOCKET_INIT";
  } else if (status == GEVLIB_ERROR_WINDOWS_SOCKET_CLOSE) {
    return "GEVLIB_ERROR_WINDOWS_SOCKET_CLOSE";
  } else if (status == GEVLIB_ERROR_SOCKET_CREATE) {
    return "GEVLIB_ERROR_SOCKET_CREATE";
  } else if (status == GEVLIB_ERROR_SOCKET_RELEASE) {
    return "GEVLIB_ERROR_SOCKET_RELEASE";
  } else if (status == GEVLIB_ERROR_SOCKET_DATA_SEND) {
    return "GEVLIB_ERROR_SOCKET_DATA_SEND";
  } else if (status == GEVLIB_ERROR_SOCKET_DATA_READ) {
    return "GEVLIB_ERROR_SOCKET_DATA_READ";
  } else if (status == GEVLIB_ERROR_SOCKET_WAIT_ACKNOWLEDGE) {
    return "GEVLIB_ERROR_SOCKET_WAIT_ACKNOWLEDGE";
  } else if (status == GEVLIB_ERROR_INVALID_INTERNAL_COMMAND) {
    return "GEVLIB_ERROR_INVALID_INTERNAL_COMMAND";
  } else if (status == GEVLIB_ERROR_INVALID_ACKNOWLEDGE) {
    return "GEVLIB_ERROR_INVALID_ACKNOWLEDGE";
  } else if (status == GEVLIB_ERROR_PREVIOUS_ACKNOWLEDGE) {
    return "GEVLIB_ERROR_PREVIOUS_ACKNOWLEDGE";
  } else if (status == GEVLIB_ERROR_INVALID_MESSAGE) {
    return "GEVLIB_ERROR_INVALID_MESSAGE";
  } else if (status == GEVLIB_ERROR_GIGE_ERROR) {
    return "GEVLIB_ERROR_GIGE_ERROR";
  }

  //===================================================
  // Device Level Status Codes (From low-level library)

  // NOTE(lindzey): I'm not sure how these differ from the above, but we
  //  do appear to get a mix of them in the "Unable to connect to camera"
  //  error state.
  else if (status == GEV_STATUS_NOT_IMPLEMENTED) {
    // !< The request isn't supported by the device.
    return "GEV_STATUS_NOT_IMPLEMENTED";
  } else if (status == GEV_STATUS_INVALID_PARAMETER) {
    // !< At least one parameter provided in the command is invalid (or out of
    // range) for the device
    return "GEV_STATUS_INVALID_PARAMETER";
  } else if (status == GEV_STATUS_INVALID_ADDRESS) {
    // !< An attempt was made to access a non existent address space location.
    return "GEV_STATUS_INVALID_ADDRESS";
  } else if (status == GEV_STATUS_WRITE_PROTECT) {
    // !< The addressed register must not be written.
    return "GEV_STATUS_WRITE_PROTECT";
  } else if (status == GEV_STATUS_BAD_ALIGNMENT) {
    // !< A badly aligned address offset or data size was specified.
    return "GEV_STATUS_BAD_ALIGNMENT";
  } else if (status == GEV_STATUS_ACCESS_DENIED) {
    // !< An attempt was made to access an address location which is
    // currently/momentary not accessible.
    return "GEV_STATUS_ACCESS_DENIED";
  } else if (status == GEV_STATUS_BUSY) {
    // !< A required resource to service the request isn't currently available.
    // The request may be retried.
    return "GEV_STATUS_BUSY";
  } else if (status == GEV_STATUS_LOCAL_PROBLEM) {
    // !< A internal problem in the device implementation occurred while
    // processing the request.
    return "GEV_STATUS_LOCAL_PROBLEM";
  } else if (status == GEV_STATUS_MSG_MISMATCH) {
    // !< Message mismatch (request and acknowledge don't match)
    return "GEV_STATUS_MSG_MISMATCH";
  } else if (status == GEV_STATUS_INVALID_PROTOCOL) {
    // !< This version of the GVCP protocol is not supported
    return "GEV_STATUS_INVALID_PROTOCOL";
  } else if (status == GEV_STATUS_NO_MSG) {
    // !< No message received, timeout.
    return "GEV_STATUS_NO_MSG";
  } else if (status == GEV_STATUS_PACKET_UNAVAILABLE) {
    // !< The request packet is not available anymore.
    return "GEV_STATUS_PACKET_UNAVAILABLE";
  } else if (status == GEV_STATUS_DATA_OVERRUN) {
    // !< Internal memory of device overrun (typically for image acquisition)
    return "GEV_STATUS_DATA_OVERRUN";
  } else if (status == GEV_STATUS_INVALID_HEADER) {
    // !< The message header is not valid. Some of its fields do not match the
    // specificiation.
    return "GEV_STATUS_INVALID_HEADER";
  } else if (status == GEV_STATUS_ERROR) {
    // !< Generic error.
    return "GEV_STATUS_ERROR";
  } else {
    return "unrecognized error!";
  }
}

}  // namespace dalsa_gige_driver

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(dalsa_gige_driver::DalsaCameraNodelet, nodelet::Nodelet)
