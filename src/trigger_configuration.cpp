/*********************************************************************
 *
 * Driver for Dalsa Gig-E cameras using GigE-V API.
 *
 * Based on prosilica driver, we inherit its BSD licnes:
 *
 * Software License Agreement (BSD License)
 *
 *  Dalsa_GigE driver Copyright (c) 2020, University of Washington;
 *  based on
 *  prosilica_driver Copyright (c) 2008, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Willow Garage / University of Washington resp.
 *     nor the names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior written
 *     permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <iostream>
#include <string>

#include "dalsa_gige_driver/dalsa_camera.h"

namespace dalsa_gige_driver {

const char *TriggerConfiguration::modeAsString() const {
  switch (mode) {
    case TriggerMode::InputLine1:
      return "Line1";
    case TriggerMode::InputLine2:
      return "Line2";
    case TriggerMode::Software:
      return "Software";
  }

  return "(unknown)";
}

const char *TriggerConfiguration::activationAsString() const {
  switch (activation) {
    case TriggerActivation::RisingEdge:
      return "RisingEdge";
    case TriggerActivation::FallingEdge:
      return "FallingEdge";
  }

  return "(unknown)";
}

}  // namespace dalsa_gige_driver
