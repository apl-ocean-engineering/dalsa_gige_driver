/*********************************************************************
 *
 * Hardware driver for Dalsa Gig-E cameras using GigE-V API.
 *
 * Based on prosilica driver, we inherit its BSD licnes:
 *
 * Software License Agreement (BSD License)
 *
 *  Dalsa_GigE driver Copyright (c) 2020, University of Washington;
 *  based on
 *  prosilica_driver Copyright (c) 2008, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Willow Garage / University of Washington resp.
 *     nor the names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior written
 *     permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "dalsa_gige_driver/dalsa_camera.h"

#include <sys/capability.h>
#include <sys/types.h>

#include "ros/assert.h"
#include "ros/console.h"

namespace dalsa_gige_driver {

DalsaCamera::DalsaCamera(GEV_CAMERA_HANDLE handle)
    : handle_(handle),
      buffers_(),
      new_frame_callback_(),
      is_running_(false),
      auto_gain_(false),
      auto_exposure_(false) {
  ;
}

DalsaCamera::~DalsaCamera() {
  receive_thread_.interrupt();
  receive_thread_.join();

  ROS_INFO_STREAM("Closing camera: " << GevCloseCamera(&handle_));

  for (auto &buf : buffers_) {
    delete buf;
  }
}

GEV_STATUS DalsaCamera::setup(const DalsaAPIConfig &conf, size_t numBuffers) {
  // Check the process' caps, warn if CAP_NET_RAW or CAP_SYS_NICE are not set
  {
    cap_t caps;
    cap_value_t cap_list[2];
    caps = cap_get_proc();
    cap_flag_value_t value;

    if (cap_get_flag(caps, CAP_NET_RAW, CAP_PERMITTED, &value) == 0) {
      if (value == CAP_SET) {
        ROS_INFO("CAP_NET_RAW capability is set");
      } else {
        ROS_WARN("CAP_NET_RAW capability NOT set");
      }
    } else {
      ROS_WARN("Unable to query capability CAP_NET_RAW");
    }

    if (cap_get_flag(caps, CAP_SYS_NICE, CAP_PERMITTED, &value) == 0) {
      if (value == CAP_SET) {
        ROS_INFO("CAP_SYS_NICE capability is set");
      } else {
        ROS_WARN("CAP_SYS_NICE capability NOT set");
      }
    } else {
      ROS_WARN("Unable to query capability CAP_SYS_NICE");
    }

    cap_free(caps);
  }

  //===================================================================================
  // Set default options for the Dalsa GigE library.
  {
    GEVLIB_CONFIG_OPTIONS options = {0};

    GevGetLibraryConfigOptions(&options);
    // Options for LOG_LEVEL = GEV_LOG_LEVEL_OFF, GEV_LOG_LEVEL_NORMAL,
    // GEV_LOG_LEVEL_WARNINGS, GEV_LOG_LEVEL_DEBUG
    options.logLevel = GEV_LOG_LEVEL_NORMAL;
    auto status = GevSetLibraryConfigOptions(&options);

    if (status != GEVLIB_OK) return status;
  }

  // Go on to adjust some API related settings (for tuning / diagnostics /
  // etc....).
  {
    GEV_CAMERA_OPTIONS camOptions = {0};

    // Adjust the camera interface options if desired (see the manual)
    GevGetCameraInterfaceOptions(handle_, &camOptions);

    // camOptions.numRetries = 3;                // Default 3 retries
    // camOptions.heartbeat_timeout_ms = 60000;		// For debugging (delay
    // camera timeout while in debugger) camOptions.heartbeat_timeout_ms = 5000;
    // // Disconnect detection (5 seconds)

    // Some tuning can be done here. (see the manual)
    // camOptions.streamFrame_timeout_ms = 1001; // Internal timeout for frame
    // reception. camOptions.streamNumFramesBuffered = 4;
    // // Buffer frames internally. camOptions.streamMemoryLimitMax =
    // 64*1024*1024;		// Adjust packet memory buffering limit.

    // If not specified, use "algorithmically calculated" value
    if (conf.packet_size > 0) {
      ROS_WARN_STREAM("Changing jumbo frames from " << camOptions.streamPktSize
                                                    << " to "
                                                    << conf.packet_size);
      camOptions.streamPktSize =
          conf.packet_size;  // Adjust the GVSP packet size.
    }

    camOptions.streamPktDelay =
        2;  // Add usecs between packets to pace arrival at NIC.

    // Assign threads to specific CPUs (affinity) - if required to prevent
    // thread migration.
    if (conf.cpu_affinity > 0) {
      ROS_WARN_STREAM("CPU affinity set to " << conf.cpu_affinity);

      // Force user to specify the affinity CPU rather than trying to
      // determine automatically
      // int numCpus = _GetNumCpus();
      // if (numCpus > 1)
      // {
      // Not clear if these two threads run in parallel or if it's one or the
      // other
      camOptions.streamThreadAffinity = conf.cpu_affinity;
      camOptions.serverThreadAffinity = conf.cpu_affinity;
      // }
    }

    // Write the adjusted interface options back.
    GEV_STATUS status = GevSetCameraInterfaceOptions(handle_, &camOptions);
    if (status != GEVLIB_OK) return status;

    // After setting options, confirm
    {
      GEV_CAMERA_OPTIONS camOptions = {0};
      GevGetCameraInterfaceOptions(handle_, &camOptions);

      ROS_INFO_STREAM("After setting camera options, packet size is "
                      << camOptions.streamPktSize);
    }
  }

  //=====================================================================
  // Get the GenICam FeatureNodeMap object and access the camera features.
  featureMap_ =
      static_cast<GenApi::CNodeMapRef *>(GevGetFeatureNodeMap(handle_));
  if (!featureMap_) return GEVLIB_ERROR_NULL_PTR;

  uint32_t format;
  uint64_t payloadSize;

  GEV_STATUS status = GevGetPayloadParameters(handle_, &payloadSize, &format);
  if (status != GEVLIB_OK) return status;

  // NODELET_INFO("Image payload = %lu; format = 0x%08x", payloadSize_,
  // format_); Buffers is resized here to make destruction cleaner.  Basically,
  // if buffers_.size() > 0, it's been allocated
  buffers_.resize(numBuffers);
  ROS_ASSERT(buffers_.size() > 0);

  std::generate(buffers_.begin(), buffers_.end(),
                [payloadSize]() { return (uint8_t *)malloc(payloadSize); });

  // Initialize a transfer with synchronous buffer handling.
  status = GevInitializeTransfer(handle_, SynchronousNextEmpty, payloadSize,
                                 buffers_.size(), buffers_.data());
  if (status != GEVLIB_OK) return status;

  // And spin off the receive thread
  receive_thread_ =
      boost::thread(boost::bind(&DalsaCamera::receiveThread, this));

  return GEVLIB_OK;
}

void DalsaCamera::setFrameCallback(ImageCallback callback) {
  new_frame_callback_ = callback;
}

void DalsaCamera::unsetFrameCallback() { new_frame_callback_.clear(); }

GEV_STATUS DalsaCamera::start(int numFrames) {
  auto status = GevStartTransfer(handle_, numFrames);
  is_running_ = true;

  return status;
}

GEV_STATUS DalsaCamera::stop() {
  GevStopTransfer(handle_);

  // receive_thread_.interrupt();
  // receive_thread_.join();

  is_running_ = false;

  return GEVLIB_OK;
}

void DalsaCamera::receiveThread() {
  // While we are still running.
  while (!boost::this_thread::interruption_requested()) {
    GEV_BUFFER_OBJECT *img = NULL;
    GEV_STATUS status = 0;

    // Wait for images to be received
    status = GevWaitForNextFrame(handle_, &img, 1000);

    if (status == GEVLIB_OK) {
      if (img != NULL) {
        ++stats.numFrames;

        if (img->status == GEV_FRAME_STATUS_RECVD) {
          if (new_frame_callback_) new_frame_callback_(img);

        } else {
          // Image had an error (incomplete (timeout/overflow/lost)).
          // Do any handling of this condition necessary.
        }
      }

    } else if (status == GEVLIB_ERROR_TIME_OUT) {
      ++stats.numTimeouts;
    }

    // Regardless, release the buffer back to the image transfer process.
    if (img) GevReleaseImage(handle_, img);
  }

  return;
}

//==========================================================================
//
// Get/set frame rate

GEV_STATUS DalsaCamera::setFrameRate(float frameRate) {
  GEV_STATUS status = GEVLIB_OK;

  float maxFrameRate = getMaxFrameRate();
  if (maxFrameRate <= 0.0) return (GEV_STATUS)maxFrameRate;

  if ((frameRate > maxFrameRate)) return GEVLIB_ERROR_ARG_INVALID;

  try {
    GenApi::CEnumerationPtr acqModeNode =
        featureMap_->_GetNode("acquisitionFrameRateControlMode");
    if (!acqModeNode) return GEVLIB_ERROR_NOT_AVAILABLE;

    if (frameRate >= 0) {
      acqModeNode->FromString("Programmable");

      GenApi::CFloatPtr frameRateNode =
          featureMap_->_GetNode("AcquisitionFrameRate");
      if (!frameRateNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      frameRateNode->SetValue(frameRate);

    } else {
      acqModeNode->FromString("MaximumSpeed");
    }
  }
  CATCH_GENAPI_ERROR(status);

  return status;
}

float DalsaCamera::getFrameRate() {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CEnumerationPtr acqModeNode =
        featureMap_->_GetNode("acquisitionFrameRateControlMode");
    if (!acqModeNode) return GEVLIB_ERROR_NOT_AVAILABLE;

    std::string acqMode = static_cast<std::string>(acqModeNode->ToString());

    if (acqMode == "MaximumSpeed") return -1;

    GenApi::CFloatPtr frameRateNode =
        featureMap_->_GetNode("AcquisitionFrameRate");
    if (!frameRateNode) return GEVLIB_ERROR_NOT_AVAILABLE;

    return frameRateNode->GetValue();
  }
  CATCH_GENAPI_ERROR(status);

  return status;
}

float DalsaCamera::getMaxFrameRate() {
  GEV_STATUS status;

  try {
    GenApi::CNodePtr maxRateNode =
        featureMap_->_GetNode("maxSustainedFrameRate");
    if (!maxRateNode) return GEVLIB_ERROR_NOT_AVAILABLE;

    GenApi::CFloatPtr maxRateFloat(maxRateNode);
    if (!maxRateFloat.IsValid()) return GEVLIB_ERROR_NOT_AVAILABLE;

    return maxRateFloat->GetValue();
  }
  CATCH_GENAPI_ERROR(status);

  return status;
}

//==========================================================================
//
// Exposure functions

float DalsaCamera::getExposure() {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CFloatPtr exposureNode = featureMap_->_GetNode("ExposureTime");
    if (!exposureNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    return exposureNode->GetValue();
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

GEV_STATUS DalsaCamera::setExposure(bool autoExposure, float exposure) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CEnumerationPtr exposureAutoNode =
        featureMap_->_GetNode("ExposureAuto");
    if (!exposureAutoNode) return GEVLIB_ERROR_NOT_AVAILABLE;

    if (autoExposure) {
      ROS_INFO_STREAM("Enabling auto exposure, with max exposure " << exposure
                                                                   << " ms");
      auto_exposure_ = true;
      exposureAutoNode->FromString("Continuous");

      const float minExposure = 100;  // milliseconds
      GenApi::CFloatPtr minValueNode =
          featureMap_->_GetNode("exposureAutoMinValue");
      if (!minValueNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      minValueNode->SetValue(minExposure);

      GenApi::CFloatPtr maxValueNode =
          featureMap_->_GetNode("exposureAutoMaxValue");
      if (!maxValueNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      maxValueNode->SetValue(exposure * 1000);

    } else {
      ROS_INFO_STREAM("Disabling auto exposure, with fixed exposure "
                      << exposure << " ms");
      auto_exposure_ = false;
      exposureAutoNode->FromString("Off");

      GenApi::CEnumerationPtr exposureModeNode =
          featureMap_->_GetNode("ExposureMode");
      if (!exposureModeNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      exposureModeNode->FromString("Timed");

      GenApi::CFloatPtr exposureNode = featureMap_->_GetNode("ExposureTime");
      if (!exposureNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      exposureNode->SetValue(exposure * 1000);
    }
  }
  CATCH_GENAPI_ERROR(status);

  // Enable/disable auto brightness depending on current auto_*_ settings
  setAutoBrightness();

  return status;
}

GEV_STATUS DalsaCamera::setAutoBrightness() {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CEnumerationPtr autoBrightnessNode =
        featureMap_->_GetNode("autoBrightnessMode");
    if (!autoBrightnessNode) return GEVLIB_ERROR_NOT_AVAILABLE;

    if (auto_gain_ || auto_exposure_) {
      ROS_INFO("Enabling auto brightness");
      autoBrightnessNode->FromString("Active");

      // Just do this by default for now
      GenApi::CEnumerationPtr autoBrightnessSequenceNode =
          featureMap_->_GetNode("autoBrightnessSequence");
      if (!autoBrightnessSequenceNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      autoBrightnessSequenceNode->FromString("Exposure_Gain_Iris");

    } else {
      ROS_INFO("Disabling auto brightness");
      autoBrightnessNode->FromString("Off");
    }
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

//==========================================================================
//
// Gain functions

const char *DalsaCamera::gainSelectorToChar(GainSelector gain) const {
  if (gain == GainSelector::Sensor) {
    return "SensorAll";
  } else {
    return "DigitalAll";
  }
}

GEV_STATUS DalsaCamera::setDigitalGain(float gain) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    // Control sensor gain right now
    GenApi::CEnumerationPtr selectorNode =
        featureMap_->_GetNode("GainSelector");
    if (!selectorNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    selectorNode->FromString("DigitalAll");

    GenApi::CFloatPtr gainNode = featureMap_->_GetNode("Gain");
    if (!gainNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    gainNode->SetValue(gain);
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

GEV_STATUS DalsaCamera::setAnalogGain(bool autoGain, float gainValue) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CEnumerationPtr gainAutoNode = featureMap_->_GetNode("GainAuto");
    if (!gainAutoNode) return GEVLIB_ERROR_NOT_AVAILABLE;

    GenApi::CEnumerationPtr selectorNode =
        featureMap_->_GetNode("GainSelector");
    if (!selectorNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    selectorNode->FromString("SensorAll");

    if (autoGain) {
      ROS_INFO_STREAM("Enabling auto gain with max value " << gainValue);
      auto_gain_ = true;

      gainAutoNode->FromString("Continuous");

      GenApi::CEnumerationPtr gainSourceNode =
          featureMap_->_GetNode("gainAutoSource");
      if (!gainSourceNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      gainSourceNode->FromString("SensorAll");

      // Fixed minimum gain value for now
      const float minGain = 1;
      GenApi::CFloatPtr minValueNode =
          featureMap_->_GetNode("gainAutoMinValue");
      if (!minValueNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      minValueNode->SetValue(minGain);

      // TODO.  Add bounds checking
      GenApi::CFloatPtr maxValueNode =
          featureMap_->_GetNode("gainAutoMaxValue");
      if (!maxValueNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      maxValueNode->SetValue(gainValue);

    } else {
      ROS_INFO_STREAM("Disabling auto gain, setting value " << gainValue);

      auto_gain_ = false;
      gainAutoNode->FromString("Off");

      GenApi::CFloatPtr gainNode = featureMap_->_GetNode("Gain");
      if (!gainNode) return GEVLIB_ERROR_NOT_AVAILABLE;
      gainNode->SetValue(gainValue);
    }
  }
  CATCH_GENAPI_ERROR(status);

  // Enable/disable auto brightness depending on current auto_*_ settings
  setAutoBrightness();

  return status;
}

float DalsaCamera::getGain(GainSelector which_gain) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CEnumerationPtr selectorNode =
        featureMap_->_GetNode("GainSelector");
    if (!selectorNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    selectorNode->FromString(gainSelectorToChar(which_gain));

    GenApi::CFloatPtr gainNode = featureMap_->_GetNode("Gain");
    if (!gainNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    return gainNode->GetValue();
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

float DalsaCamera::getAnalogGain() { return getGain(GainSelector::Sensor); }

float DalsaCamera::getDigitalGain() { return getGain(GainSelector::Digital); }

//==========================================================================
//
// ColorGain (color balance) functions

const char *DalsaCamera::colorSelectorToChar(ColorSelector gain) const {
  if (gain == ColorSelector::Blue) {
    return "Blue";
  } else if (gain == ColorSelector::Green) {
    return "Green";
  } else {
    return "Red";
  }
}

GEV_STATUS DalsaCamera::setColorGain(ColorSelector color, float gain) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    // Control sensor gain right now
    GenApi::CEnumerationPtr selectorNode =
        featureMap_->_GetNode("BalanceWhiteAuto");
    if (!selectorNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    selectorNode->FromString("Off");

    selectorNode = featureMap_->_GetNode("BalanceRatioSelector");
    if (!selectorNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    selectorNode->FromString(colorSelectorToChar(color));

    GenApi::CFloatPtr gainNode = featureMap_->_GetNode("BalanceRatio");
    if (!gainNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    gainNode->SetValue(gain);
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

GEV_STATUS DalsaCamera::setRedGain(float gain) {
  return setColorGain(ColorSelector::Red, gain);
}

GEV_STATUS DalsaCamera::setBlueGain(float gain) {
  return setColorGain(ColorSelector::Blue, gain);
}

float DalsaCamera::getColorGain(ColorSelector color) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    // Control sensor gain right now
    GenApi::CEnumerationPtr selectorNode =
        featureMap_->_GetNode("BalanceRatioSelector");
    if (!selectorNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    selectorNode->FromString(colorSelectorToChar(color));

    GenApi::CFloatPtr gainNode = featureMap_->_GetNode("BalanceRatio");
    if (!gainNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    return gainNode->GetValue();
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

float DalsaCamera::getRedGain() { return getColorGain(ColorSelector::Red); }

float DalsaCamera::getBlueGain() { return getColorGain(ColorSelector::Blue); }

float DalsaCamera::getGreenGain() { return getColorGain(ColorSelector::Green); }

//==========================================================================
//
// Get/set gamma correction

GEV_STATUS DalsaCamera::setGamma(float gamma) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    // For now, gamma == 1.0 is synonomous with "turn gamma off"
    if (gamma == 1.0) {
      GenApi::CEnumerationPtr lutModePtr = featureMap_->_GetNode("lutMode");
      if (!lutModePtr) {
        ROS_INFO("Disabling LUT mode");
        return GEVLIB_ERROR_NOT_AVAILABLE;
      }
      lutModePtr->FromString("Off");

    } else {
      GenApi::CEnumerationPtr lutModePtr = featureMap_->_GetNode("lutMode");
      if (!lutModePtr) {
        ROS_INFO("Enabling LUT mode");
        return GEVLIB_ERROR_NOT_AVAILABLE;
      }
      lutModePtr->FromString("Active");

      GenApi::CEnumerationPtr lutTypePtr = featureMap_->_GetNode("lutType");
      if (!lutTypePtr) {
        ROS_INFO("  ... setting LUT to gamma correction");
        return GEVLIB_ERROR_NOT_AVAILABLE;
      }
      lutTypePtr->FromString("GammaCorrection");

      GenApi::CFloatPtr gammaNode = featureMap_->_GetNode("gammaCorrection");
      if (!gammaNode) {
        ROS_INFO("  ... setting gamma");
        return GEVLIB_ERROR_NOT_AVAILABLE;
      }
      gammaNode->SetValue(gamma);
    }
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

float DalsaCamera::getGamma() {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CFloatPtr gammaNode = featureMap_->_GetNode("gammaCorrection");
    if (!gammaNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    return gammaNode->GetValue();
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

//==========================================================================
//
// Set outputs/triggers

GEV_STATUS DalsaCamera::setFlip(bool flip_x, bool flip_y) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CBooleanPtr flipXNode = featureMap_->_GetNode("ReverseX");
    *flipXNode = flip_x;

    GenApi::CBooleanPtr flipYNode = featureMap_->_GetNode("ReverseY");
    *flipYNode = flip_y;
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

bool DalsaCamera::getXFlip() {
  GenApi::CBooleanPtr flipXNode = featureMap_->_GetNode("ReverseX");
  return (*flipXNode)();
}

bool DalsaCamera::getYFlip() {
  GenApi::CBooleanPtr flipYNode = featureMap_->_GetNode("ReverseY");
  return (*flipYNode)();
}

//==========================================================================
//
// Set outputs/triggers

GEV_STATUS DalsaCamera::setOutput(const OutputConfiguration &config) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    // And configure the hardware?
    GenApi::CEnumerationPtr lineSelectorNode =
        featureMap_->_GetNode("LineSelector");
    lineSelectorNode->FromString(config.lineAsString());

    // This doesn't work...
    //
    //     GenApi::CEnumerationPtr lineInverterNode =
    //     featureMap_->_GetNode("LineInverter"); lineInverterNode->FromString(
    //     "Off" );

    GenApi::CEnumerationPtr outputLineSource =
        featureMap_->_GetNode("outputLineSource");
    outputLineSource->FromString(config.sourceAsString());
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

GEV_STATUS DalsaCamera::setTrigger(const TriggerConfiguration &config) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    if (config.mode == TriggerConfiguration::TriggerMode::FixedRate ||
        config.mode == TriggerConfiguration::TriggerMode::Streaming) {
      GenApi::CEnumerationPtr triggerSelectorNode =
          featureMap_->_GetNode("TriggerSelector");
      GenApi::CEnumerationPtr triggerModeNode =
          featureMap_->_GetNode("TriggerMode");

      triggerSelectorNode->FromString("FrameStart");
      triggerModeNode->FromString("Off");

      triggerSelectorNode->FromString("AcquisitionStart");
      triggerModeNode->FromString("Off");

      if (config.mode == TriggerConfiguration::TriggerMode::FixedRate) {
        return setFrameRate(config.frameRate);
      } else {
        return setFrameRate(-1);  // Set to max frame rate
      }

    } else {
      GenApi::CEnumerationPtr triggerSelectorNode =
          featureMap_->_GetNode("TriggerSelector");
      GenApi::CEnumerationPtr triggerModeNode =
          featureMap_->_GetNode("TriggerMode");

      triggerSelectorNode->FromString("AcquisitionStart");
      triggerModeNode->FromString("Off");

      triggerSelectorNode->FromString("FrameStart");
      triggerModeNode->FromString("On");

      GenApi::CEnumerationPtr triggerSourceNode =
          featureMap_->_GetNode("TriggerSource");
      triggerSourceNode->FromString(config.modeAsString());

      GenApi::CEnumerationPtr triggerActivationNode =
          featureMap_->_GetNode("TriggerActivation");
      triggerActivationNode->FromString(config.activationAsString());

      // And configure the hardware?
      GenApi::CEnumerationPtr lineSelectorNode =
          featureMap_->_GetNode("LineSelector");
      lineSelectorNode->FromString(config.modeAsString());

      // std::cout << "Setting LineInverter" << std::endl;
      //       GenApi::CEnumerationPtr lineInverterNode =
      //       featureMap_->_GetNode("LineInverter");
      //       lineInverterNode->FromString( "False" );
    }
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

//==========================================================================
//
//

GEV_STATUS DalsaCamera::setTurboDrive(bool doTd) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    // And configure the hardware?
    GenApi::CEnumerationPtr turboDriveNode =
        featureMap_->_GetNode("TurboTransferMode");
    turboDriveNode->FromString(doTd ? "True" : "False");
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

int DalsaCamera::getPacketSize() {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CIntegerPtr packetSizeNode =
        featureMap_->_GetNode("GevSCPSPacketSize");
    if (!packetSizeNode) return GEVLIB_ERROR_NOT_AVAILABLE;
    return packetSizeNode->GetValue();
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

GEV_STATUS DalsaCamera::setPacketSize(int packetSize) {
  GEV_STATUS status = GEVLIB_OK;

  if (packetSize < 0) return GEVLIB_ERROR_ARG_INVALID;

  try {
    GenApi::CIntegerPtr packetSizeNode =
        featureMap_->_GetNode("GevSCPSPacketSize");
    if (!packetSizeNode) return GEVLIB_ERROR_NOT_AVAILABLE;

    packetSizeNode->SetValue(packetSize);
  }
  CATCH_GENAPI_ERROR(status);

  return status;
}

GEV_STATUS DalsaCamera::setThroughputLimit(unsigned int limit) {
  GEV_STATUS status = GEVLIB_OK;

  try {
    GenApi::CEnumerationPtr turboNode =
        featureMap_->_GetNode("transferTurboMode");
    turboNode->FromString("Off");

    GenApi::CEnumerationPtr throughputLimitNode =
        featureMap_->_GetNode("DeviceLinkThroughputLimitMode");

    if (limit == 0) {
      throughputLimitNode->FromString("Off");

      GenApi::CIntegerPtr packetDelayNode = featureMap_->_GetNode("GevSCPD");
      packetDelayNode->SetValue(1);

    } else {
      throughputLimitNode->FromString("On");

      GenApi::CIntegerPtr throughputNode =
          featureMap_->_GetNode("DeviceLinkThroughputLimit");
      throughputNode->SetValue(limit);
    }
  }
  CATCH_GENAPI_ERROR(status);
  return status;
}

//==========================================================================
//
// Unimplemented functions / dead code

// Boost gain
// {
//   GEV_STATUS status = GEVLIB_OK;
//
//   try{
//
//   GenApi::CEnumerationPtr gainAutoNode = featureMap_->_GetNode("GainAuto");
//   gainAutoNode->FromString("Off");
//
//     GenApi::CEnumerationPtr gainSelectorNode =
//     featureMap_->_GetNode("GainSelector"); if( gainSelectorNode ) {
//       gainSelectorNode->FromString( "SensorAll" );
//
//       // GenApi::CFloatPtr gainNode = featureMap_->_GetNode("Gain");
//       // if( gainNode ) {
//       //       gainNode->SetValue(4);
//       // }
//
//       GenApi::CIntegerPtr gainNode = featureMap_->_GetNode("GainRaw");
//       if( gainNode ) {
//             gainNode->SetValue(0);
//       }
//     }
//
//
//   }
//   CATCH_GENAPI_ERROR(status);
// }

// void DalsaCamera::removeEvents()
// {
//     // clear all events
//     PvAttrUint32Set(handle_, "EventsEnable1", 0);
// }
//
// tPvFrame* DalsaCamera::grab(unsigned long timeout_ms)
// {
//     assert( FSTmode_ == Software );
//     tPvFrame* frame = &frames_[0];
//
//     CHECK_ERR( PvCommandRun(handle_, "FrameStartTriggerSoftware"), "Couldn't
//     trigger capture" ); CHECK_ERR( PvCaptureWaitForFrameDone(handle_, frame,
//     timeout_ms), "couldn't capture frame");
//     // don't requeue if capture has stopped
//     if (frame->Status == ePvErrUnplugged || frame->Status == ePvErrCancelled
//     )
//     {
//       return NULL;
//     }
//     CHECK_ERR( PvCaptureQueueFrame(handle_, frame, NULL), "Couldn't queue
//     frame");
//
//     if (frame->Status == ePvErrSuccess)
//         return frame;
//     if (frame->Status == ePvErrDataMissing || frame->Status == ePvErrTimeout)
//     {
//         //! recommanding after an error seems to cause a sequence error if
//         next command is too fast
//         boost::this_thread::sleep(boost::posix_time::millisec(50));
//         return NULL;
//     }
//     else
//         throw std::runtime_error("Unknown error grabbing frame");
//
//     return frame;
// }
//
// void DalsaCamera::setExposure(unsigned int val, AutoSetting isauto)
// {
//   CHECK_ERR( PvAttrEnumSet(handle_, "ExposureMode", autoValues[isauto]),
//              "Couldn't set exposure mode" );
//
//   if (isauto == Manual)
//     CHECK_ERR( PvAttrUint32Set(handle_, "ExposureValue", val),
//                "Couldn't set exposure value" );
// }
//
// void DalsaCamera::setGain(unsigned int val, AutoSetting isauto)
// {
//   /// @todo Here and in setWhiteBalance, would be better to split off
//   setGainMode etc.
//   /// I didn't take into account there are cameras that don't support auto
//   gain, auto white balance. if (PvAttrIsAvailable(handle_, "GainMode") ==
//   ePvErrSuccess)
//   {
//     CHECK_ERR( PvAttrEnumSet(handle_, "GainMode", autoValues[isauto]),
//                "Couldn't set gain mode" );
//   }
//
//   if (isauto == Manual)
//     CHECK_ERR( PvAttrUint32Set(handle_, "GainValue", val),
//                "Couldn't set gain value" );
// }
//
// void DalsaCamera::setWhiteBalance(unsigned int blue, unsigned int red,
// AutoSetting isauto)
// {
//   if (PvAttrIsAvailable(handle_, "WhitebalMode") == ePvErrSuccess)
//   {
//     CHECK_ERR( PvAttrEnumSet(handle_, "WhitebalMode", autoValues[isauto]),
//                "Couldn't set white balance mode" );
//   }
//
//   if (isauto == Manual)
//   {
//       if(hasAttribute("WhitebalValueBlue"))
//       {
//         CHECK_ERR( PvAttrUint32Set(handle_, "WhitebalValueBlue", blue),
//                    "Couldn't set white balance blue value" );
//       }
//       if(hasAttribute("WhitebalValueRed"))
//       {
//         CHECK_ERR( PvAttrUint32Set(handle_, "WhitebalValueRed", red),
//                    "Couldn't set white balance red value" );
//       }
//   }
// }
//
// void DalsaCamera::setRoi(unsigned int x, unsigned int y,
//                     unsigned int width, unsigned int height)
// {
//   CHECK_ERR( PvAttrUint32Set(handle_, "RegionX", x),
//              "Couldn't set region x (left edge)" );
//   CHECK_ERR( PvAttrUint32Set(handle_, "RegionY", y),
//              "Couldn't set region y (top edge)" );
//   CHECK_ERR( PvAttrUint32Set(handle_, "Width", width),
//              "Couldn't set region width" );
//   CHECK_ERR( PvAttrUint32Set(handle_, "Height", height),
//              "Couldn't set region height" );
// }
//
// void DalsaCamera::setRoiToWholeFrame()
// {
//   tPvUint32 min_val, max_val;
//   CHECK_ERR( PvAttrUint32Set(handle_, "RegionX", 0),
//              "Couldn't set region x (left edge)" );
//   CHECK_ERR( PvAttrUint32Set(handle_, "RegionY", 0),
//              "Couldn't set region y (top edge)" );
//   CHECK_ERR( PvAttrRangeUint32(handle_, "Width", &min_val, &max_val),
//              "Couldn't get range of Width attribute" );
//   CHECK_ERR( PvAttrUint32Set(handle_, "Width", max_val),
//              "Couldn't set region width" );
//   CHECK_ERR( PvAttrRangeUint32(handle_, "Height", &min_val, &max_val),
//              "Couldn't get range of Height attribute" );
//   CHECK_ERR( PvAttrUint32Set(handle_, "Height", max_val),
//              "Couldn't set region height" );
// }
//
// void DalsaCamera::setBinning(unsigned int binning_x, unsigned int binning_y)
// {
//   // Permit setting to "no binning" on cameras without binning support
//   if (!hasAttribute("BinningX") && binning_x == 1 && binning_y == 1)
//     return;
//
//   CHECK_ERR( PvAttrUint32Set(handle_, "BinningX", binning_x),
//              "Couldn't set horizontal binning" );
//   CHECK_ERR( PvAttrUint32Set(handle_, "BinningY", binning_y),
//              "Couldn't set vertical binning" );
// }

}  // namespace dalsa_gige_driver
