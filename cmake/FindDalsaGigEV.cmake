# Modified from FindGIGEV packaged with CMake
# Original license below:
# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#.rst:
# FindGIGEV
# --------
#
# Find the Dalse GigE-V includes and library.  Also finds the GenICam
# library which is installed by the DALSA driver
#
# IMPORTED Targets
# ^^^^^^^^^^^^^^^^
#
# This module defines :prop_tgt:`IMPORTED` target ``GIGEV::GIGEV``, if
# GIGEV has been found.
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module defines the following variables:
#
# ::
#
#   GIGEV_INCLUDE_DIRS   - where to find GIGEV.h, etc.
#   GIGEV_LIBRARIES      - List of libraries when using GIGEV.
#   GIGEV_FOUND          - True if GIGEV found.
#
#
# Hints
# ^^^^^
#
# A user may set ``GIGEV_ROOT`` to a GIGEV installation root to tell this
# module where to look.

set(_GIGEV_SEARCHES)

# Search GIGEV_ROOT first if it is set.
if(GIGEV_ROOT)
  set(_GIGEV_SEARCH_ROOT PATHS ${GIGEV_ROOT} NO_DEFAULT_PATH)
  list(APPEND _GIGEV_SEARCHES _GIGEV_SEARCH_ROOT)
endif()

# Normal search.
set(_GIGEV_SEARCH_NORMAL
  PATHS "[HKEY_LOCAL_MACHINE\\SOFTWARE\\GnuWin32\\GIGEV;InstallPath]"
        "$ENV{PROGRAMFILES}/GIGEV"
        "/usr/dalsa/GigeV"
)
list(APPEND _GIGEV_SEARCHES _GIGEV_SEARCH_NORMAL)

# Look for includes
foreach(search ${_GIGEV_SEARCHES})
  find_path(GIGEV_INCLUDE_DIR NAMES gev_linux.h ${${search}} PATH_SUFFIXES include)
endforeach()

# # Allow GIGEV_LIBRARY to be set manually, as the location of the GIGEV library
if(NOT GIGEV_LIBRARY)
  foreach(search ${_GIGEV_SEARCHES})
    find_library(GIGEV_LIBRARY NAMES GevApi ${${search}} PATH_SUFFIXES lib)
  endforeach()
endif()

# mark_as_advanced(GIGEV_INCLUDE_DIR)
#
# if(GIGEV_INCLUDE_DIR AND EXISTS "${GIGEV_INCLUDE_DIR}/GIGEV.h")
#     file(STRINGS "${GIGEV_INCLUDE_DIR}/GIGEV.h" GIGEV_H REGEX "^#define GIGEV_VERSION \"[^\"]*\"$")
#
#     string(REGEX REPLACE "^.*GIGEV_VERSION \"([0-9]+).*$" "\\1" GIGEV_VERSION_MAJOR "${GIGEV_H}")
#     string(REGEX REPLACE "^.*GIGEV_VERSION \"[0-9]+\\.([0-9]+).*$" "\\1" GIGEV_VERSION_MINOR  "${GIGEV_H}")
#     string(REGEX REPLACE "^.*GIGEV_VERSION \"[0-9]+\\.[0-9]+\\.([0-9]+).*$" "\\1" GIGEV_VERSION_PATCH "${GIGEV_H}")
#     set(GIGEV_VERSION_STRING "${GIGEV_VERSION_MAJOR}.${GIGEV_VERSION_MINOR}.${GIGEV_VERSION_PATCH}")
#
#     # only append a TWEAK version if it exists:
#     set(GIGEV_VERSION_TWEAK "")
#     if( "${GIGEV_H}" MATCHES "GIGEV_VERSION \"[0-9]+\\.[0-9]+\\.[0-9]+\\.([0-9]+)")
#         set(GIGEV_VERSION_TWEAK "${CMAKE_MATCH_1}")
#         string(APPEND GIGEV_VERSION_STRING ".${GIGEV_VERSION_TWEAK}")
#     endif()
#
#     set(GIGEV_MAJOR_VERSION "${GIGEV_VERSION_MAJOR}")
#     set(GIGEV_MINOR_VERSION "${GIGEV_VERSION_MINOR}")
#     set(GIGEV_PATCH_VERSION "${GIGEV_VERSION_PATCH}")
# endif()
#
# include(${CMAKE_CURRENT_LIST_DIR}/FindPackageHandleStandardArgs.cmake)

INCLUDE( FindPackageHandleStandardArgs )
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GIGEV REQUIRED_VARS GIGEV_LIBRARY GIGEV_INCLUDE_DIR )
#                                        VERSION_VAR GIGEV_VERSION_STRING)
#
if(GIGEV_FOUND)

  set(GIGEV_INCLUDE_DIRS ${GIGEV_INCLUDE_DIR})

  if(NOT GIGEV_LIBRARIES)
    set(GIGEV_LIBRARIES ${GIGEV_LIBRARY})
  endif()

  if(NOT TARGET GIGEV::GIGEV)
    add_library(GIGEV::GIGEV UNKNOWN IMPORTED)
    set_target_properties(GIGEV::GIGEV PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${GIGEV_INCLUDE_DIRS}")

      set_property(TARGET GIGEV::GIGEV APPEND PROPERTY
        IMPORTED_LOCATION "${GIGEV_LIBRARY}")
    endif()

endif()
