/*********************************************************************
 *
 * Driver for Dalsa Gig-E cameras using GigE-V API.
 *
 * Based on prosilica driver, we inherit its BSD licnes:
 *
 * Software License Agreement (BSD License)
 *
 *  Dalsa_GigE driver Copyright (c) 2020, University of Washington;
 *  based on
 *  prosilica_driver Copyright (c) 2008, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Willow Garage / University of Washington resp.
 *     nor the names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior written
 *     permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#pragma once

#include <GenApi/GenApi.h>
#include <gevapi.h>

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <string>
#include <vector>

#include "dalsa_gige_driver/output_configuration.h"
#include "dalsa_gige_driver/trigger_configuration.h"
#include "expected.hpp"

namespace dalsa_gige_driver {

struct DalsaCameraFactory;

class DalsaCamera {
 public:
  static const size_t NUM_BUFFERS = 8;

  friend struct DalsaCameraFactory;

  typedef boost::shared_ptr<DalsaCamera> shared_ptr;

  struct DalsaAPIConfig {
    DalsaAPIConfig() : packet_size(0), cpu_affinity(-1) {}

    int packet_size;
    int cpu_affinity;
  };

  DalsaCamera() = delete;
  DalsaCamera(const DalsaCamera &) = delete;

  ~DalsaCamera();

  /// Typedef for userspace callback when a new image arrives
  typedef boost::function<void(GEV_BUFFER_OBJECT *)> ImageCallback;

  /// Set the frame callback.
  /// \param callback Function pointer for user callback.
  ///
  void setFrameCallback(ImageCallback callback);

  /// Clear the frame callback.
  ///
  void unsetFrameCallback();

  /// \brief Start capture
  ///
  /// \param @numFrames -1 means "forever"
  ///
  GEV_STATUS start(int numFrames = -1);

  /// \brief Stop capture.
  ///
  GEV_STATUS stop();

  /// \return true if the camera is currently running
  bool isRunning() const { return is_running_; }

  //=====================================================================
  //
  // Functions to get/set frame rate
  //

  /// frameRate in fps;  -1 means "maximum"
  /// n.b. 0 is a valid value, it means "don't run camera"
  GEV_STATUS setFrameRate(float frameRate);
  float getFrameRate();
  float getMaxFrameRate();

  //=====================================================================
  //
  // Functions to get/set gain and color balance
  //

  enum class GainSelector { Sensor, Digital };

  const char *gainSelectorToChar(GainSelector gain) const;

  float getGain(GainSelector selector);
  float getAnalogGain();
  float getDigitalGain();

  /// Sets the analog gain.
  ///    If autoGain == true, then gainValue is the max allowed gain
  ///    If autoGain == false, then gainValue is the specified gain
  ///
  GEV_STATUS setAnalogGain(bool autoGain, float gainValue);
  GEV_STATUS setDigitalGain(float value);

  bool getAutoGain() { return auto_gain_; }

  enum class ColorSelector { Red, Blue, Green };

  const char *colorSelectorToChar(ColorSelector color) const;

  float getColorGain(ColorSelector color);
  float getRedGain();
  float getBlueGain();
  float getGreenGain();

  GEV_STATUS setColorGain(ColorSelector color, float gain);
  GEV_STATUS setRedGain(float gain);
  GEV_STATUS setBlueGain(float gain);

  // GEV_STATUS setAutoBrightnessExposureLimits(float maxValue, float
  // minValue=500); GEV_STATUS setAutoBrightnessGainLimits(float maxValue, float
  // minValue=1);

  //=====================================================================
  //
  // Functions to get/set exposure
  //

  float getExposure();
  bool getAutoExposure() { return auto_exposure_; }

  /// Sets the exposure in milliseconds
  ///    If autoExposure == true, then exposure is the max allowed
  ///    If autoExposure == false, then exposure is the specified exposure
  ///
  GEV_STATUS setExposure(bool autoExposure, float exposure);

  //=====================================================================
  //
  // Functions to get/set trigger and output functions
  //

  GEV_STATUS setTrigger(const TriggerConfiguration &config);
  GEV_STATUS setOutput(const OutputConfiguration &output);

  //=====================================================================
  //
  // Functions to get/set gamma correction
  //

  /// Set the camera's gamma correction.
  ///
  /// \param gamma The desired gamma correction.  Gamma == 1.0 means "disable
  /// gamma correction" \return a GEV status value
  GEV_STATUS setGamma(float gamma);

  /// Request the current gamma correction.
  ///
  /// \return The current camera gamma value or a GEV_STATUS
  float getGamma();

  //=====================================================================
  //
  // Functions to get/set image flipping
  //

  /// Set the image flipping
  ///
  ///
  GEV_STATUS setFlip(bool flip_x, bool flip_y);

  /// Request the current X flipping from the camera
  /// \return The current Y-flipping configuration
  bool getXFlip();

  /// Request the current Y flipping from the camera
  /// \return The current Y-flipping configuration
  bool getYFlip();

  //=====================================================================
  //
  // Functions to get/set various transmission properties
  //

  GEV_STATUS setTurboDrive(bool doTd);

  /// \brief Set throughput limit in camera
  /// \param limit  Limit in ... ??   A value of "0" means "unlimited"
  GEV_STATUS setThroughputLimit(unsigned int limit);

  GEV_STATUS getPacketSize();
  GEV_STATUS setPacketSize(int packetSize);

  struct Stats {
    Stats() : numTimeouts(0), numFrames(0) { ; }

    int numTimeouts, numFrames;
  } stats;

 private:
  /// Private constructor, used by the DalsaCameraFactory functions
  DalsaCamera(GEV_CAMERA_HANDLE handle);

  /// Common setup / configuration code.  Unlike the constructor, this can fail
  /// \return a GEV_STATUS value
  GEV_STATUS setup(const DalsaAPIConfig &config,
                   size_t bufferSize = NUM_BUFFERS);

  /// \brief Function which runs within receive thread
  void receiveThread();

  /// \brief Handle to receive thread
  boost::thread receive_thread_;

  /// \brief Dalsa GigE-V handle for camera
  GEV_CAMERA_HANDLE handle_;
  GenApi::CNodeMapRef *featureMap_;

  /// Buffers for data exchange with GevApi
  std::vector<uint8_t *> buffers_;

  /// Track whether start() has been called.
  bool is_running_;

  // Set/unset auto brightness depending on current value of auto_gain_ and
  // auto_exposure_
  GEV_STATUS setAutoBrightness();

  // Track state so we can automatically enable/disable brightness as required
  bool auto_gain_, auto_exposure_;

  /// \brief Function pointer to user callback when a new frame arrives
  ImageCallback new_frame_callback_;
};

}  // namespace dalsa_gige_driver
